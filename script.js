/*
 Теоретичні питання.
1. Визначити клавішу, яку натиснув користувач ми можемо, проглянувши об'єкт події клавіатури, наприклад "keyup":
  document.addEventListener("keyup", (event) => {
    console.log(event.code);
    });
   В об'єкті події клавіатури "keyup" ми можемо знайти властивість "code: 'клавіша'", яка визначає клавішу, нажату і відпущену користувачем.
2. Різниця між event.code() та event.key() полягае в тому, що перша властивість визначає клавішу, нажату користувачем, а друга -
   напис на цій клавіші. Наприклад у нас може бути 2 клавіші з однаковим написом "shift" або "ctrl". Або наприклад одна клавіша
   може мати різний напис в розкладках клавіатури різних країн.
3.  Існує три події клавіатури - "keydown", "keyup", "keypress". Перша подія спрацьовує, коли користувач тільки натискає на клавішу.
   "keypress" спрацьовує зразу після першої події  "keydown" і якщо держати клавішу натисненою, то ці 2 події будуть спрацьовувать
   постійно, доки користувач не отпусте клавішу, тільки потім спрацює подія "keyup". Остання подія завжди буває один раз
   при одноразовому використанні якоїсь клавіши, 2 другі події "keydown" та "keypress" можуть багаторазово чередуватись при
   довгому натисненні на клавішу.
 */
'use strict'
const btn = document.getElementsByClassName('btn');
   for(let i = 0; i <= btn.length; i++){
        document.addEventListener('keydown', (e) => {
            if(e.key === btn[i].textContent || e.key === btn[i].textContent.toLowerCase()){
                btn[i].style.backgroundColor = 'blue';
            } else {
                   btn[i].style.backgroundColor = 'black';
            }
        });
    }



